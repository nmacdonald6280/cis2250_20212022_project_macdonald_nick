package info.hccis.tutoradministration.ui.studentList;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import info.hccis.tutoradministration.R;

import info.hccis.tutoradministration.bo.Student;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final List<Student> mValues;
    private final StudentListFragment.OnListFragmentInteractionListener mListener;
    //private List<Customer> filteredCustomerList;

    public RecyclerViewAdapter(List<Student> items, StudentListFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_student_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mInitialView.setText(mValues.get(position).getStudentInitial());
        holder.mFullNameView.setText(mValues.get(position).getFirstName() + " " + mValues.get(position).getLastName());
        holder.mEmailView.setText(mValues.get(position).getEmail());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mInitialView;
        public final TextView mFullNameView;
        public final TextView mEmailView;

        public Student mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mInitialView = (TextView) view.findViewById(R.id.circleInitial);
            mFullNameView = (TextView) view.findViewById(R.id.textViewFullName);
            mEmailView = (TextView) view.findViewById(R.id.textViewEmail);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mFullNameView.toString() + "'" + mEmailView.toString();
        }
    }
}

package info.hccis.tutoradministration.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import info.hccis.tutoradministration.bo.Student;

@Database(entities = {Student.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract StudentDAO studentDAO();

}

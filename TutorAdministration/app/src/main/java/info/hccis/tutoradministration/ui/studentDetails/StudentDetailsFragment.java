package info.hccis.tutoradministration.ui.studentDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import info.hccis.tutoradministration.R;
import info.hccis.tutoradministration.bo.Student;
import info.hccis.tutoradministration.ui.studentList.StudentListFragment;

public class StudentDetailsFragment extends Fragment {

    private StudentDetailsViewModel studentDetailsViewModel;
    private Student student;
    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvPhoneNumber;
    private TextView tvEmail;
    private Button btnAddContact;
    private Button btnShareInfo;
    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
          BJM 20200202
          modified by nick macdonald 20220419
          Note how the arguments are passed from the activity to this fragment.  The
          student object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String studentJson = getArguments().getString("student");
            Gson gson = new Gson();
            student = gson.fromJson(studentJson, Student.class);
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        studentDetailsViewModel =
                new ViewModelProvider(this).get(StudentDetailsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_student_details, container, false);
        studentDetailsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        tvFirstName = root.findViewById(R.id.textViewStudentFirstName);
        tvLastName = root.findViewById(R.id.textViewStudentLastName);
        tvPhoneNumber = root.findViewById(R.id.textViewPhoneNumber);
        tvEmail = root.findViewById(R.id.textViewStudentEmail);

        tvFirstName.setText(student.getFirstName());
        tvLastName.setText(student.getLastName());
        tvPhoneNumber.setText(student.getPhoneNumber());
        tvEmail.setText(student.getEmail());

        /*
          taken from research of last year 2019/2020
          When the add button is clicked, will pass the student object to the activity implementation
          method.  The MainActivity will then take care of adding the contact.
         */
        btnAddContact = root.findViewById(R.id.btnAddContact);
        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionAddContact(student);
            }
        });

        /*
          taken from research of last year 2019/2020
          Similar to the add contact button, to send an email will notify the MainActivity and
          let it take care of sending the email with the information passed in the student.
         */
        btnShareInfo = root.findViewById(R.id.btnShareInfo);
        btnShareInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionShareInfo(student);
            }
        });

        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StudentListFragment.OnListFragmentInteractionListener) {
            mListener = (StudentDetailsFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteractionAddContact(Student item);

        void onFragmentInteractionShareInfo(Student student);
    }
}
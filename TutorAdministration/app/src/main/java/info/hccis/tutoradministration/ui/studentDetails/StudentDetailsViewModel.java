package info.hccis.tutoradministration.ui.studentDetails;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class StudentDetailsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public StudentDetailsViewModel() {
        mText = new MutableLiveData<>();
        //mText.setValue("This is feedback fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
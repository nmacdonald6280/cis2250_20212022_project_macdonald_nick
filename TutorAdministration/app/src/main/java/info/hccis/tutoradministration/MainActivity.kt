package info.hccis.tutoradministration

import com.facebook.CallbackManager.Factory.create
import androidx.appcompat.app.AppCompatActivity
import info.hccis.tutoradministration.ui.studentList.StudentListFragment.OnListFragmentInteractionListener
import info.hccis.tutoradministration.ui.studentDetails.StudentDetailsFragment.OnFragmentInteractionListener
import androidx.navigation.ui.AppBarConfiguration
import android.os.Bundle
import info.hccis.tutoradministration.util.NotificationApplication
import com.facebook.CallbackManager
import info.hccis.tutoradministration.R
import info.hccis.tutoradministration.util.AirplaneModeReceiver
import android.content.IntentFilter
import android.content.Intent
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import info.hccis.tutoradministration.MainActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.navigation.ui.NavigationUI
import androidx.room.Room
import info.hccis.tutoradministration.dao.MyAppDatabase
import androidx.navigation.NavController
import info.hccis.tutoradministration.bo.Student
import com.google.gson.Gson
import android.provider.ContactsContract
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.navigation.Navigation

class MainActivity : AppCompatActivity(), OnListFragmentInteractionListener, OnFragmentInteractionListener {
    private var mAppBarConfiguration: AppBarConfiguration? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        NotificationApplication.setContext(this)
        super.onCreate(savedInstanceState)
        val callbackManager = create()
        setContentView(R.layout.activity_main)
        //Creating the instance of airplane mode changed
        val receiver = AirplaneModeReceiver()
        //Creating an instance of the intent
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)
        this.registerReceiver(receiver, filter)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        /*
         * Set onClickListener when click on fab to navigate to the student fragment where to add a student
         *
         * @author nickmac
         * @since 20220310
         */
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Add a Student", Snackbar.LENGTH_LONG).show()
            navController!!.navigate(R.id.nav_student)
        }
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_about, R.id.nav_student,
                R.id.nav_student_list, R.id.nav_tools, R.id.nav_student_details, R.id.nav_help,
                R.id.nav_map)
                .setDrawerLayout(drawer)
                .build()
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController!!, mAppBarConfiguration!!)
        NavigationUI.setupWithNavController(navigationView, navController!!)
        myAppDatabase = Room.databaseBuilder(applicationContext, MyAppDatabase::class.java, "studentsdb").allowMainThreadQueries().build()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        return (NavigationUI.onNavDestinationSelected(item, navController)
                || super.onOptionsItemSelected(item))
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        return (NavigationUI.navigateUp(navController, mAppBarConfiguration!!)
                || super.onSupportNavigateUp())
    }

    /**
     * This method will be used in the student fragment when the user clicks on a row of the
     * student recyclerview.  This method will transfer the user to a details fragment.  The
     * id of the student will be passed to the fragment and used to load the correct student details.
     * Will use the arraylist associated with the recyclerview.
     *
     * @param item student
     * @author BJM
     * @since 20200124 Modified by Nick MacDonald 20220310
     */
    override fun onListFragmentInteraction(item: Student) {
        Log.d("ma", "item communicated from fragment: $item")
        val bundle = Bundle()
        val gson = Gson()
        bundle.putString("student", gson.toJson(item))

        /*
          BJM 20200202
          Use the navigation controller object stored as an attribute of the main
          activity to navigate the ui to the customer detail fragment.
        */navController!!.navigate(R.id.nav_student_details, bundle)
    }

    /**
     * This is the interaction from the details fragment.  I will want to add a new contact when
     * this button is pressed.
     *
     * @param item
     * @author BJM Modified by Nick Macdonald 20220310
     * @since 20200203
     */
    override fun onFragmentInteractionAddContact(item: Student) {
        /* Code to add a contact */
        val intent = Intent(ContactsContract.Intents.Insert.ACTION)
        intent.type = ContactsContract.RawContacts.CONTENT_TYPE
        intent.putExtra(ContactsContract.Intents.Insert.NAME, item.firstName)
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, item.phoneNumber)
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, item.email)
        startActivity(intent)
    }

    /**
     * This is the interaction from the details fragment.  I will want to share contact when
     * this button is pressed.
     *
     * @param item
     * @author BJM Modified by nick macdonald 20220419
     * @since 20200203
     */
    override fun onFragmentInteractionShareInfo(item: Student) {
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_SUBJECT, "tutor admin notification")
        email.putExtra(Intent.EXTRA_TEXT, item.toString())

        //need this to prompts email client only
        email.type = "message/rfc822"
        startActivity(Intent.createChooser(email, "Choose an Email student :"))
    }

    companion object {
        @JvmField
        var myAppDatabase: MyAppDatabase? = null

        @JvmStatic
        var navController: NavController? = null
            private set
    }
}
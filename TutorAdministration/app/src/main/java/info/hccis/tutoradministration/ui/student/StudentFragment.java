package info.hccis.tutoradministration.ui.student;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import info.hccis.tutoradministration.R;
import info.hccis.tutoradministration.bo.Student;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StudentFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private StudentViewModel studentViewModel;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextPhoneNumber;
    private EditText editTextEmail;
    private int programCode;
    private Spinner spinner;
    private Button buttonSubmit;

    private StudentRepository studentRepository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        studentViewModel =
                new ViewModelProvider(this).get(StudentViewModel.class);
        View root = inflater.inflate(R.layout.fragment_student, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        studentViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
        editTextFirstName = root.findViewById(R.id.editTextTextFirstName);
        editTextLastName = root.findViewById(R.id.editTextTextLastName);
        editTextPhoneNumber = root.findViewById(R.id.editTextTextPhoneNumber);
        editTextEmail = root.findViewById(R.id.editTextTextEmail);
        Spinner spinner = (Spinner) root.findViewById(R.id.programSpinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.programs_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        studentRepository = studentRepository.getInstance();

        //submit button
        buttonSubmit = root.findViewById(R.id.btnSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Student s = new Student(
                        editTextFirstName.getText().toString(),
                        editTextLastName.getText().toString(),
                        editTextPhoneNumber.getText().toString(),
                        editTextEmail.getText().toString(),
                        programCode
                );

                studentRepository.getStudentService().createStudent(s).enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> r) {
                        Toast.makeText(getContext(), "Student " + r.body().getFirstName() + "" + r.body().getLastName() + " successfully added!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {
                        Toast.makeText(getContext(), "Error Adding Student: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }
        });

        return root;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (position) {
            case 0:
                break;
            case 1:
                programCode = 1;
                break;
            case 2:
                programCode = 2;
                break;
            case 3:
                programCode = 3;
                break;
            case 4:
                programCode = 4;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
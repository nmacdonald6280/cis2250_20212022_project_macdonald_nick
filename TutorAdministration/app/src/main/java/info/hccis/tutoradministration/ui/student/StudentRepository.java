package info.hccis.tutoradministration.ui.student;

import info.hccis.tutoradministration.util.JsonStudentApi;
import info.hccis.tutoradministration.util.Util;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Author: Mariana Alkabalan modified by nick macdonald 20220419
 * Date: 27.03.2021
 * Reference: https://learntodroid.com/how-to-send-json-data-in-a-post-request-in-android/
 */
public final class StudentRepository {
    private static StudentRepository instance;

    private JsonStudentApi jsonStudentApi;

    public static StudentRepository getInstance() {
        if (instance == null) {
            instance = new StudentRepository();
        }
        return instance;
    }

    public StudentRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.STUDENT_BASE_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonStudentApi = retrofit.create(JsonStudentApi.class);
    }

    public JsonStudentApi getStudentService() {
        return jsonStudentApi;
    }
}

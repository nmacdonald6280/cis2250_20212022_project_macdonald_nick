package info.hccis.tutoradministration.dao;

import android.content.ContentProvider;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import info.hccis.tutoradministration.bo.Student;

@Dao
public interface StudentDAO {
    @Insert
    public long add(Student student);

    @Update
    public void update(Student student);

    @Delete
    public void delete(Student student);

    @Query("select * from student")
    public List<Student> get();

    @Query("select * from student where id=:id")
    public Student get(int id);

    @Query("delete from student")
    public void deleteAll();
}

package info.hccis.tutoradministration.ui.about;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import info.hccis.tutoradministration.R;

import static info.hccis.tutoradministration.MainActivity.getNavController;

public class AboutFragment extends Fragment {

    private AboutViewModel aboutViewModel;
    private ImageButton imageButtonMap;
    private Button btnGoToFacebook;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        aboutViewModel = new ViewModelProvider(this).get(AboutViewModel.class);
        View root = inflater.inflate(R.layout.fragment_about, container, false);
        aboutViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        /*
         * Set on click listener on Map Image Button - call the Nav Controller from the main activity to navigate to MapFragment
         * Date: 15/02/2021
         * Author: Mariana Alkabalan modified by nick macdonald 20220416
         * Purpose: Tutor Administration project
         */
        imageButtonMap = root.findViewById(R.id.imageButtonMap);
        imageButtonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNavController().navigate(R.id.nav_map);
            }
        });

        /*
         * Set on click listener on Mail Image Button - call the Nav Controller from the main activity to navigate to MailFragment
         * Date: 15/02/2021
         * Author: Mariana Alkabalan modified by nick macdonald 20220419
         * Purpose: Tutor Administion project
         */
        btnGoToFacebook = root.findViewById(R.id.btnGoToFacebook);
        btnGoToFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFacebookPageId("100735549299100");
            }
        });

        return root;
    }

    private void getFacebookPageId(String id) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page//" + id));
            startActivity(i);
        } catch (ActivityNotFoundException ex) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/" + id));
            startActivity(i);
        }
    }
}
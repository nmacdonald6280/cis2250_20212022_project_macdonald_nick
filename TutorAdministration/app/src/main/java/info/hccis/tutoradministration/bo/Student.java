package info.hccis.tutoradministration.bo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import info.hccis.tutoradministration.util.Util;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "student")
public class Student implements Serializable {
    public static final String STUDENT_BASE_API = Util.BASE_SERVER + "/api/StudentService/";
    private static final long serialVersionUID = 1L;
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @SerializedName("fName")
    @ColumnInfo(name = "firstName")
    private String firstName;

    @SerializedName("lName")
    @ColumnInfo(name = "lastName")
    private String lastName;

    @SerializedName("pNum")
    @ColumnInfo(name = "phoneNumber")
    private String phoneNumber;

    @SerializedName("eMail")
    @ColumnInfo(name = "emailAddress")
    private String email;

    @SerializedName("prog")
    @ColumnInfo(name = "programCode")
    private Integer programCode;

    public Student() {
    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(String firstName, String lastName, String phoneNumber, String email, Integer programCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.programCode = programCode;
    }

    public Student(String firstName, String lastName, String phoneNumber, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public Student(Integer id) {
        this.id = id;
    }

    public Student(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getProgramCode() {
        return programCode;
    }

    public void setProgramCode(Integer programCode) {
        this.programCode = programCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStudentInitial() {
        String initial = "";
        initial = firstName.charAt(0) + "" + lastName.charAt(0);
        return initial.toUpperCase();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ID " + id + " Name: " + firstName + "" + lastName + " Email: " + email + " Phone number " + phoneNumber;
    }
}


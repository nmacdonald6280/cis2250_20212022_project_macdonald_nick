package info.hccis.tutoradministration.util;

import java.util.List;

import info.hccis.tutoradministration.bo.Student;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonStudentApi {

    /**
     * This abstract method to be created to allow retrofit to get list of students
     * @return List of students
     * @since 20200202
     * @author BJM (with help from the retrofit research.
     */

    @GET("student")
    Call<List<Student>> getStudents();
    @POST("student")
    Call<Student> createStudent(@Body Student student);
}

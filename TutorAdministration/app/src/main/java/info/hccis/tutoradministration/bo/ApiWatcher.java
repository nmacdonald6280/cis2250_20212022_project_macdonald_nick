package info.hccis.tutoradministration.bo;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.tutoradministration.ui.studentList.StudentListFragment;
import info.hccis.tutoradministration.util.JsonStudentApi;
import info.hccis.tutoradministration.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApiWatcher class will be used as a background thread which will monitor the api. It will notify
 * the ui activity if the number of rows changes.
 *
 * @author BJM modified by Nick MacDonald 20220419
 * @since 20210329
 */

public class ApiWatcher extends Thread {

    private int lengthLastCall = -1;  //Number of rows returned

    //The activity is passed in to allow the runOnUIThread to be used.
    private Activity activity = null;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();

        try {
            do {
                Thread.sleep(10000); //Check api every 10 seconds

                //************************************************************************
                // A lot of this code will access the api and if if notes that the number of orders
                // has changed, will notify the view order fragment that the data is changed.
                //************************************************************************
                Log.d("ma", "running");

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Util.STUDENT_BASE_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonStudentApi jsonStudentApi = retrofit.create(JsonStudentApi.class);

                //Create a list of students.
                Call<List<Student>> call = jsonStudentApi.getStudents();
                call.enqueue(new Callback<List<Student>>() {

                    @Override
                    public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {
                        List<Student> students = new ArrayList();
                        if (!response.isSuccessful()) {
                            Log.d("ma", "not successful response from rest for students Code=" + response.code());
                        } else {
                            students = response.body();
                            int lengthThisCall = students.size();

                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                            } else if (lengthThisCall != lengthLastCall) {
                                //data has changed
                                Log.d("ma", "Data has changed");
                                lengthLastCall = lengthThisCall;

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        StudentListFragment.notifyDataChanged("Update - the data has changed");
                                    }
                                });

                            } else {
                                Log.d("ma", "Data has not changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Student>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("ma", "api call failed");
                        Log.d("ma", t.getMessage());
                    }
                });

            } while (true);

        } catch (InterruptedException e) {
            Log.d("ma", "Thread interrupted.  Stopping in the thread.");
        }
    }
}


package info.hccis.tutoradministration.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.tutoradministration.MainActivity;
import info.hccis.tutoradministration.R;
import info.hccis.tutoradministration.ui.studentList.StudentListFragment;
import info.hccis.tutoradministration.util.JsonStudentApi;
import info.hccis.tutoradministration.util.NotificationUtil;
import info.hccis.tutoradministration.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 */
public class StudentContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Student> STUDENTS = new ArrayList<Student>();

    public static void reloadStudentsInRoom(List<Student> students)
    {
        MainActivity.myAppDatabase.studentDAO().deleteAll();
        for(Student current : students)
        {
            MainActivity.myAppDatabase.studentDAO().add(current);
        }
        Log.d("ma","loading students from Room");
    }

    public static List<Student> getStudentsFromRoom()
    {
        Log.d("ma","Loading students from Room");
        List<Student> studentsBack = MainActivity.myAppDatabase.studentDAO().get();
        Log.d("ma","Number of students loaded: " + studentsBack.size());
        for(Student current : studentsBack)
        {
            Log.d("ma",current.toString());
        }
        return studentsBack;
    }

    /**
     * Load the students.  This method will use the rest service to provide the data.
     * This is the list which is used to back the RecyclerView.
     *
     */
    public static void loadStudents(Activity context) {

        Log.d("ma", "Accessing api at:" + Util.STUDENT_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.STUDENT_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonStudentApi jsonStudentApi = retrofit.create(JsonStudentApi.class);

        //Create a list of studentss.
        Call<List<Student>> call = jsonStudentApi.getStudents();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<Student>>() {

            @Override
            public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {

                List<Student> students;

                if(!response.isSuccessful())
                {
                    Log.d("MA-Rest","Unsuccessful response from rest: " + response.code());
                    SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);
                    boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room),false);
                    if(preferToLoad)
                    {
                        students = getStudentsFromRoom();
                        NotificationUtil.sendNotification("Students loaded", "(" + students.size() + ") Students loaded from Room");
                    } else {
                        NotificationUtil.sendNotification("Students not loaded", "Cannot load Studentss - Connection Error");
                        return;
                    }
                } else {
                    students = response.body();
                    NotificationUtil.sendNotification("Students loaded", "(" + students.size() + ") Students loaded from API service");

                    //Have successfully connected.  Reload the student database.
                    reloadStudentsInRoom(students);

                }

                Log.d("ma", "data back from service call #returned=" + students.size());

                //**********************************************************************************
                // Now that we have the students, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                StudentContent.STUDENTS.clear();
                StudentContent.STUDENTS.addAll(students);

                //**********************************************************************************
                // The StudentListFragment has a recyclerview which is used to show the customer list.
                // This recyclerview is backed by the customer list in the StudentContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                StudentListFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
                //Remove the progress bar from the view.
                StudentListFragment.clearProgressBarVisibility();

            }

            @Override
            public void onFailure(Call<List<Student>> call, Throwable t) {
                // If the api call failed, give a notification to the user.
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());

                SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);

                boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room),false);

                StudentContent.STUDENTS.clear();
                if (preferToLoad) {
                    StudentContent.STUDENTS.addAll(getStudentsFromRoom());
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Students loaded", "(" + StudentContent.STUDENTS.size() + ") Students loaded from room");
                } else {
                    StudentContent.STUDENTS.clear();
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Students not loaded", "Cannot load Students - Connection Error");
                }

                StudentListFragment.clearProgressBarVisibility();
                StudentListFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
            }
        });
    }
}
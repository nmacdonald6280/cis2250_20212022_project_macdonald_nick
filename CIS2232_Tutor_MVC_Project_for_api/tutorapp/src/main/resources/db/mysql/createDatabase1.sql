DROP DATABASE IF EXISTS cis2232_tutor;
CREATE DATABASE cis2232_tutor;
use cis2232_tutor;

CREATE TABLE CodeType (codeTypeId int(3) COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT 'This tables holds the code types that are available for the application';

INSERT INTO CodeType (CodeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(2, 'Status Types', 'Status Types FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, ''),
(3, 'Skill Types', 'Skill Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(4, 'Hour Types', 'Hour Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(5, 'Hour Rates', 'Hour Rates FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(6, 'Employee Types', 'Employee Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
(7, 'Program Code', 'Program Code FR', sysdate(), '', CURRENT_TIMESTAMP, '');

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  sortOrder int(3) DEFAULT NULL COMMENT 'Sort order if applicable',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT='This will hold code values for the application.';

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');
INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 2, 'Admin', 'Admin', 'Admin', 'Admin', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(2, 1, 'Active', 'Active', 'Active', 'Active', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 2, 'Not Active', 'Not Active', 'Not Active', 'Not Active', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(3, 1, 'Java', 'Java', 'Java', 'Java', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 2, 'Javascript', 'Javascript', 'Javascript', 'Javascript', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 3, 'C#', 'C#', 'C#', 'C#', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 4, 'Database', 'Database', 'Database', 'Database', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 5, 'Networking', 'Networking', 'Networking', 'Networking', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, frenchDescription, englishDescriptionShort,frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(4, 1, 'Individual', 'Individual', 'Individual', 'Individual', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 2, 'Small Group', 'Small Group', 'Small Group', 'Small Group', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 3, 'Large Group', 'Large Group', 'Large Group', 'Large Group', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(5, 1, '15', '15', '15', '15', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(5, 2, '18', '18', '18', '18', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(5, 3, '20', '20', '20', '20', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, frenchDescription, englishDescriptionShort,frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(6, 1, 'Second Year Student', 'Second Year Student', 'Second Year', 'Second Year', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(6, 2, 'Past Graduate', 'Past Graduate', 'Graduate', 'Graduate', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, frenchDescription, englishDescriptionShort,frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(7, 1, 'Computer Information Systems', 'Computer Information Systems', 'CIS', 'CIS', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(7, 2, 'Business Administration', 'Business Administration', 'Bus Admin', 'Bus Admin', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(7, 3, 'Computer Networking', 'Computer Networking', 'CNT', 'CNT', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(7, 4, 'Electronic Engineering', 'Electronic Engineering', 'EE', 'EE', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

CREATE TABLE UserAccess (
  userAccessId int(3) NOT NULL,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  name varchar(128),
  userAccessStatusCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #2',
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  createdDateTime datetime DEFAULT NULL COMMENT 'When user was created.'
);

ALTER TABLE CodeType
  ADD PRIMARY KEY (codeTypeId);

ALTER TABLE CodeValue
  ADD PRIMARY KEY (codeTypeId,codeValueSequence);
--  ADD KEY codeTypeId (codeTypeId);

ALTER TABLE UserAccess
  ADD PRIMARY KEY (userAccessId),
  ADD KEY userTypeCode (userTypeCode);

ALTER TABLE CodeType
  MODIFY codeTypeId int(3) NOT NULL COMMENT 'This is the primary key for code types';

ALTER TABLE CodeValue
  MODIFY codeValueSequence int(3) NOT NULL;

ALTER TABLE UserAccess
  MODIFY userAccessId int(3) NOT NULL AUTO_INCREMENT;



drop table if exists TutorSkill;
drop table if exists StudentSkill;
drop table if exists InvoiceHour;

drop table if exists Tutor;
drop table if exists Student;
drop table if exists Invoice;

CREATE TABLE Tutor(
id int(5) COMMENT 'This is the primary key',
firstName varchar(40) COMMENT 'First name',
lastName varchar(40) COMMENT 'Last name',
phoneNumber varchar(10) COMMENT '1112223333',
emailAddress varchar(100) COMMENT 'Email Address',
address varchar(100) COMMENT 'Mailing Address',
employeeType int(3) COMMENT 'Code type#6'
) COMMENT 'This table holds person data';

ALTER TABLE Tutor
  ADD PRIMARY KEY (id);
ALTER TABLE Tutor
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE TutorSkill(
id int(5),
tutorId int(5) COMMENT 'FK to Tutor',
skillType int(3) COMMENT 'Code Type 3 (Skill where help can be provided)'
) COMMENT 'This table holds person data';

ALTER TABLE TutorSkill
  ADD PRIMARY KEY (id);
ALTER TABLE TutorSkill
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;


CREATE TABLE Student(
id int(5),
firstName varchar(40) COMMENT 'First name',
lastName varchar(40) COMMENT 'Last name',
phoneNumber varchar(10) COMMENT '1112223333',
emailAddress varchar(100) COMMENT 'Email Address',
programCode int(3) COMMENT 'Code Type#7 Student program'
) COMMENT 'This table holds tutor data';

ALTER TABLE Student
  ADD PRIMARY KEY (id);
ALTER TABLE Student
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;


CREATE TABLE StudentSkill(
id int(5),
studentId int(5) COMMENT 'FK to Student',
skillType int(3) COMMENT 'Code Type 3 (Skill where help is needed)' 
) COMMENT 'This table holds student data';

ALTER TABLE StudentSkill
  ADD PRIMARY KEY (id);
ALTER TABLE StudentSkill
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE Invoice(
id int(5),
tutorId int(5) COMMENT 'FK to Tutor',
startDate varchar(10) COMMENT 'yyyy-mm-dd',
endDate varchar(10) COMMENT 'yyyy-mm-dd'
) COMMENT 'This table holds invoice data';

ALTER TABLE Invoice
  ADD PRIMARY KEY (id);

ALTER TABLE Invoice
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE InvoiceHour(
id int(5) COMMENT 'This is the primary key',
invoiceId int(5) COMMENT 'FK to Invoice',
numberOfHours int(3) COMMENT 'Number of hours worked',
hoursTypeCode int(3) COMMENT 'Code Type=4'
) COMMENT 'This table holds the hour details for the invoice';

ALTER TABLE InvoiceHour
  ADD PRIMARY KEY (id);

ALTER TABLE InvoiceHour
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

insert into Tutor values(0, "John", "Rogers", "9023145566","jrogers@gmail.com","24 MacBeth Cres. Charlottetown",1);
insert into Tutor values(0, "Bill", "Akatti", "9023146677","bakatti@gmail.com","75 Grafton St. Charlottetown",2);
insert into Tutor values(0, "George", "Ryan", "9023147789","gryan@gmail.com","12 Hillsborough St. Charlottetown",2);
insert into Tutor values(0, "Mariana", "Clark", "9023141245","mclark@hollandcollege.com","44 Water St. Charlottetown",1);
insert into Tutor values(0, "Renee", "Riley", "9029401234","jrogers@gmail.com","23 Water St. Summerside",2);

insert into Student values(0, "Ben", "Leonard", "9023148712","bleonard@hollandcollege.com",1);
insert into Student values(0, "Adline", "Sloan", "9023141234","asloan@hollandcollege.com",3);
insert into Student values(0, "Bert", "Brothers", "9023885522","bbrothers123@hollandcollege.com",3);
insert into Student values(0, "Hunter", "Ripley", "9023145432","rripley@hollandcollege.com",2);
insert into Student values(0, "Andres", "MacDonald", "9023148899","amacdonald12123@hollandcollege.com",1);

insert into Invoice values(0, 1, "2020-01-01","2020-01-31");
insert into Invoice values(0, 2, "2020-01-01","2020-01-31");
insert into Invoice values(0, 3, "2020-01-01","2020-01-31");
insert into Invoice values(0, 1, "2020-02-01","2020-01-31");
insert into Invoice values(0, 2, "2020-02-01","2020-02-15");
insert into Invoice values(0, 3, "2020-02-01","2020-02-15");

insert into InvoiceHour values (0,1,3,2); 
insert into InvoiceHour values (0,1,2,3);
insert into InvoiceHour values (0,2,3,1);
insert into InvoiceHour values (0,2,3,2);
insert into InvoiceHour values (0,2,3,3);
insert into InvoiceHour values (0,3,5,1);
insert into InvoiceHour values (0,4,10,1);
insert into InvoiceHour values (0,5,6,2);
insert into InvoiceHour values (0,5,2,3);
insert into InvoiceHour values (0,6,1,1);

insert into StudentSkill values (0,1,3);
insert into StudentSkill values (0,1,4);
insert into StudentSkill values (0,1,5);
insert into StudentSkill values (0,2,1);
insert into StudentSkill values (0,2,2);
insert into StudentSkill values (0,3,3);
insert into StudentSkill values (0,4,1);
insert into StudentSkill values (0,4,2);
insert into StudentSkill values (0,4,3);
insert into StudentSkill values (0,4,4);
insert into StudentSkill values (0,4,5);
insert into StudentSkill values (0,5,1);
insert into StudentSkill values (0,5,2);

insert into TutorSkill values (0,1,3);
insert into TutorSkill values (0,1,5);
insert into TutorSkill values (0,2,1);
insert into TutorSkill values (0,2,3);
insert into TutorSkill values (0,4,1);
insert into TutorSkill values (0,4,2);
insert into TutorSkill values (0,4,3);
insert into TutorSkill values (0,5,1);
insert into TutorSkill values (0,5,2);




package info.hccis.tutoring.bo;

import info.hccis.tutoring.dao.StudentDAO;
import info.hccis.tutoring.entity.Student;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 * @modified fhm 20211018
 */
public class StudentBO {

	public ArrayList<Student> selectStudentsBySkillId(int id) {

		StudentDAO studentDAO = new StudentDAO();
		return studentDAO.selectStudentsBySkillId(id);

	}
}

package info.hccis.tutoring.dao;

import info.hccis.tutoring.entity.Code;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DAO Class used to get courses.
 *
 * @author bjm
 * @since 20211008
 * @modified fhm 20211018
 */
public class CodeDAO {

	private static ResultSet rs;
	private static Connection conn = null;

	public CodeDAO() {

		String propFileName = "application";
		ResourceBundle rb = ResourceBundle.getBundle(propFileName);
		String connectionString = rb.getString("spring.datasource.url");
		String userName = rb.getString("spring.datasource.username");
		String password = rb.getString("spring.datasource.password");

		try {
			conn = DriverManager.getConnection(connectionString, userName, password);
		} catch (SQLException e) {
			Logger.getLogger(CodeDAO.class.getName()).log(Level.SEVERE, null, e);
		}

	}

	/**
	 * Select all courses.
	 *
	 * @since 20210924
	 * @author BJM
	 * @modified fhm 20211018
	 * @modified fhm 20211122 Modified to resolve issue #8.
	 */
	public ArrayList<Code> getCode(byte code) {
		Statement stmt;
		ArrayList<Code> courses = new ArrayList();
		try {
			stmt = conn.createStatement();
			String query = "SELECT codevalue.codeValueSequence, codevalue.englishDescription FROM "
					+ "codevalue WHERE "
					+ "codevalue.codeTypeId = "+code+" ORDER BY "
					+ "codevalue.codeValueSequence;";
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			while (rs.next()) {
				courses.add(new Code(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			Logger.getLogger(CodeDAO.class.getName()).log(Level.SEVERE, null, e);
		}
		return courses;
	}

}

package info.hccis.tutoring.repositories;

import info.hccis.tutoring.entity.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Simple repository for database interaction
 *
 * @author Alonso Briones
 * @since 10/21/2021
 */
@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {

}
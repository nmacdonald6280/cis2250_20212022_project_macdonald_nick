package info.hccis.tutoring.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Class used to represent a student.
 *
 * @author fhm
 * @since 20211018
 * @modified 20211030 fhm Now supports repository.
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

	//The names of the variables might not match what is in the DB. This is for backwards compatibility (report).
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private int id;
	@Size(max = 40)
	@Column(name = "firstName")
	private String fName;
	@Size(max = 40)
	@Column(name = "lastName")
	private String lName;
	@Size(max = 10)
	@Column(name = "phoneNumber")
	private String pNum;
	@Size(max = 100)
	@Column(name = "emailAddress")
	private String eMail;
	@Column(name = "programCode")
	private Integer prog;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getpNum() {
		return pNum;
	}

	public void setpNum(String pNum) {
		this.pNum = pNum;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public Integer getProg() {
		return prog;
	}

	public void setProg(Integer prog) {
		this.prog = prog;
	}

}

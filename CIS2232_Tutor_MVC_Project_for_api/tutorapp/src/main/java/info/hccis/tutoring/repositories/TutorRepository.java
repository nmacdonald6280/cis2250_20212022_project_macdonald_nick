package info.hccis.tutoring.repositories;

import info.hccis.tutoring.entity.Tutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * repository for database interaction
 *
 * @author Carter Phillips
 * @since 2021-10-14
 */
@Repository
public interface TutorRepository extends CrudRepository<Tutor, Integer> {

    
    //Tutor findByTutorId(int id);
    
    
    
}
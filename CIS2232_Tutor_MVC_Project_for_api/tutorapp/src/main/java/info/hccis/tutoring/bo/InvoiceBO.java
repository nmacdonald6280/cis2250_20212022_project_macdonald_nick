package info.hccis.tutoring.bo;

import info.hccis.tutoring.dao.InvoiceDAO;
import info.hccis.tutoring.entity.Invoice;
import info.hccis.tutoring.entity.InvoiceReport;
import java.util.ArrayList;

/**
 * Invoice class to handle InvoiceDAO methods 
 * @author Alonso Briones
 */
public class InvoiceBO {

    /**
     * This method creates a InvoiceDAO object to then call and return the showByName method
     * which shows all invoices based on a tutor first name and last name
     * @param fname tutor first name
     * @param lname tutor last name
     * @return ArrayList<InvoiceReport>
     * @author Alonso Briones
     */
    public ArrayList<InvoiceReport> showInvoiceByName(String fullName) {
        System.out.println("Showing invoice by tutor name...");
        InvoiceDAO invoiceDAO = new InvoiceDAO();
        
        return invoiceDAO.showByName(fullName);
    }
}

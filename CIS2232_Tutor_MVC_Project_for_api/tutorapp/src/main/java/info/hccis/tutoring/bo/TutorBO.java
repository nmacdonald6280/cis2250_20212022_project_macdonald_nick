package info.hccis.tutoring.bo;


import info.hccis.tutoring.dao.TutorDAO;
import java.util.ArrayList;

/**
 * Contains the business logic for the Tutor Skill report
 * @author Carter Phillips
 * @since 20211001
 */
public class TutorBO {
    
    public ArrayList<String> getTutorInformationForTutorSkill(String name){
        TutorDAO tutorDAO = new TutorDAO();
        return tutorDAO.selectTutorInformationByTutorSkill(name);
    }

    //reportStudentCourseDAO = reportTutorDAO
}

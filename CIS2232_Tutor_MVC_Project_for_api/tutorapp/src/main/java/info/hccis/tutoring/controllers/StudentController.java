package info.hccis.tutoring.controllers;

import info.hccis.tutoring.bo.CodeBO;
import info.hccis.tutoring.entity.Student;
import info.hccis.tutoring.repositories.StudentRepository;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the student functionality of the site
 *
 * @since 20211014
 * @author CIS2232
 * @modified 20211030 Modified to make it meet requirements for S3.
 */
@Controller
@RequestMapping("/student")
public class StudentController {

	private final StudentRepository _sr;

	@Autowired
	public StudentController(StudentRepository sr) {
		_sr = sr;
	}

	/**
	 * Lists all students
	 *
	 * @param model
	 * @return a list of all students
	 * @since 20211014
	 * @author BJM
	 * @modified 20211030 fhm Slight modification to allow courses to be shown.
	 */
	@RequestMapping("")
	public String list(Model model) {
		model.addAttribute("students", _sr.findAll());
		CodeBO codeValueBO = new CodeBO();
		model.addAttribute("courses", codeValueBO.getCode((byte)7));

		return "student/list";
	}

	/**
	 * Page to add new student
	 *
	 * @param model
	 * @return add
	 * @since 20211015
	 * @author BJM
	 * @modified 20211030 fhm Modification to allow dropdown instead of typing
	 * id of program.
	 */
	@RequestMapping("/add")
	public String add(Model model) {
		Student student = new Student();
		model.addAttribute("student", student);
		CodeBO codeValueBO = new CodeBO();
		model.addAttribute("courses", codeValueBO.getCode((byte)7));
		return "student/add";
	}

	/**
	 * Submit method that processes add and edit and any form submission
	 *
	 * @param model
	 * @param student Student being added or modified
	 * @param bindingResult Result of SQL
	 * @return add with errors or student
	 * @since 20211015
	 * @author CIS2232
	 * @modified 20211030 fhm Redirect instead, dropdown for program selection.
	 */
	@RequestMapping("/submit")
	public String submit(Model model, @Valid @ModelAttribute("student") Student student, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			System.out.println("Validation error");
			for (ObjectError error : bindingResult.getAllErrors()) {
				System.out.println(error.getDefaultMessage());
			}
			CodeBO codeValueBO = new CodeBO();
			model.addAttribute("courses", codeValueBO.getCode((byte)7));
			return "student/add";
		}

		_sr.save(student);
		return "redirect:/student";
	}

	/**
	 * Page to edit member
	 *
	 * @param id ID of student
	 * @param model
	 * @return add if student is valid, otherwise, student
	 * @since 20191212
	 * @author Fred Campos
	 * @modified 20211030 fhm Modified to work as add page (dropdown for program
	 * selection). See above.
	 */
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable int id, Model model) {
		Optional student = _sr.findById(id);
		if (student.isPresent()) {
			model.addAttribute("student", student.get());
			CodeBO codeValueBO = new CodeBO();
			model.addAttribute("courses", codeValueBO.getCode((byte)7));
			return "student/add";
		}
		return "redirect:/student";
	}

	/**
	 * Page to delete a student
	 *
	 * @param id ID of student
	 * @return student
	 * @since 20211025
	 * @author Fred Campos
	 */
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable int id) {
		_sr.deleteById(id);
		return "redirect:/student";
	}
}

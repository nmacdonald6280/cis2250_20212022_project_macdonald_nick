package info.hccis.tutoring.controllers;

import info.hccis.tutoring.bo.CodeBO;
import info.hccis.tutoring.bo.StudentBO;
import info.hccis.tutoring.entity.Code;
import info.hccis.tutoring.entity.Student;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/reports")
public class StudentReportController {

	@RequestMapping("/studentReport")
	public String ReportStudents(Model model, @ModelAttribute("studentReport") info.hccis.tutoring.entity.StudentReport studentReport) {

		StudentBO rcfsbo = new StudentBO();
		ArrayList<Student> students = rcfsbo.selectStudentsBySkillId(studentReport.getId());

		CodeBO codeValueBO = new CodeBO();
		ArrayList<Code> courses = codeValueBO.getCode((byte)3);

		studentReport.setStudents(students);
		studentReport.setCourses(courses);

		model.addAttribute("reportForStudents", studentReport);

		return "reports/studentReport";
	}

}

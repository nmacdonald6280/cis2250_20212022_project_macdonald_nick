package info.hccis.tutoring.rest;

import com.google.gson.Gson;
import info.hccis.tutoring.entity.Student;
import info.hccis.tutoring.repositories.StudentRepository;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author bjmaclean
 * @since N/A
 * @modified 20211122 fhm Remove redundant code, set correct HTTP methods
 * (https://www.tutorialspoint.com/restful/restful_quick_guide.htm, says POST
 * should be used for creation and PUT for updating), uncomment and fix provided
 * code. Also removed placeholder comments. Remember: 400 = Bad Request 204 = No
 * Content (not an error unlike 404) 201 = Created 200 = OK
 */
@Path("/StudentService/student")
public class StudentService {

    private final StudentRepository _sr;

    @Autowired
    public StudentService(StudentRepository _sr) {
        this._sr = _sr;
    }

    @GET
    @Produces("application/json")
    public ArrayList<Student> getAll() {
        return (ArrayList<Student>) _sr.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getStudentById(@PathParam("id") int id) throws URISyntaxException {
        Optional<Student> student = _sr.findById(id);
        if (student.isPresent()) {
            return Response.status(200).entity(student).build();
        }
        return Response.status(204).build();
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response createStudent(String studentJson) {
        try {
            return Response.status(201).entity(save(studentJson)).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
        } catch (AllAttributesNeededException aane) {
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteStudent(@PathParam("id") int id) throws URISyntaxException {

        Optional<Student> student = _sr.findById(id);
        if (student.isPresent()) {
            _sr.deleteById(id);
            return Response.status(200).build();
        }
        return Response.status(204).build();
    }

    @PUT
    @Produces("application/json")
    @Consumes("application/json")
    public Response updateStudent(String studentJson) throws URISyntaxException {
        //Ensure we do not "add" to the database. We strictly want this to update.
        Gson gson = new Gson();
        Student current = gson.fromJson(studentJson, Student.class);
        System.out.println(_sr.findById(current.getId()));
        Optional<Student> student = _sr.findById(current.getId());
        if (!student.isPresent()) {
            return Response.status(204).build();
        }
        try {
            return Response.status(200).entity(save(studentJson)).header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
        } catch (AllAttributesNeededException aane) {
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }

    public String save(String json) throws AllAttributesNeededException {
        Gson gson = new Gson();
        Student student = gson.fromJson(json, Student.class);
        student = _sr.save(student);
        return gson.toJson(student);
    }
}

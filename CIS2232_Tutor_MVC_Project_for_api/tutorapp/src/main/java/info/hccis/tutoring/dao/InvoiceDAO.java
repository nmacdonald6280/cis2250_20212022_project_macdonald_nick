package info.hccis.tutoring.dao;

import info.hccis.tutoring.entity.InvoiceReport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles all the data access from the invoices to the database
 * @author Alonso Briones
 */
public class InvoiceDAO {

    private static ResultSet rs;
    private static Statement stmt = null;
    private static Connection conn = null;

    public InvoiceDAO() {
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String connectionString = rb.getString("spring.datasource.url");
        String userName = rb.getString("spring.datasource.username");
        String password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(connectionString, userName, password);
        } catch (Exception e) {
            System.out.println("Error");
        }
    }

    /**
     * This method shows all the invoices based on a tutor name
     * @param fname tutor first name
     * @param lname tutor last name
     * @author Alonso Briones
     */
    public ArrayList<InvoiceReport> showByName(String fullName) {
        ArrayList<InvoiceReport> invoiceReports = new ArrayList();
        String[] tutorName = fullName.split(" ");
        String fname = tutorName[0];
        String lname = tutorName[1];
        
        try {
            stmt = conn.createStatement();
            String query = "select t.id, ih.numberOfHours, cv.englishDescription, i.startDate, i.endDate\n"
                    + "from tutor t, invoice i, invoicehour ih, codevalue cv\n"
                    + "where t.id = i.tutorId\n"
                    + "and i.id = ih.invoiceId\n"
                    + "and cv.codeTypeId = 4\n"
                    + "and ih.hoursTypeCode = cv.codeValueSequence\n"
                    + "and t.firstName = '" + fname + "'\n"
                    + "and t.lastName = '" + lname + "'";
            System.out.println(query);
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }

        try {
            while (rs.next()) {
                InvoiceReport invoiceReport = new InvoiceReport();
                invoiceReport.setTutorFirstName(fname);
                invoiceReport.setTutorLastName(lname);
                invoiceReport.setId(Integer.valueOf(rs.getString(1)));
                invoiceReport.setNumHours(Integer.valueOf(rs.getString(2)));
                invoiceReport.setEngDescription(rs.getString(3) + " Hours");
                invoiceReport.setStartDate(rs.getString(4));
                invoiceReport.setEndDate(rs.getString(5));
                invoiceReports.add(invoiceReport);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InvoiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return invoiceReports;
    }
}

package info.hccis.tutoring.entity;

import java.util.ArrayList;

/**
 * Class to hold report data
 * @author Carter Phillips
 * @since 20211001
 */
public class TutorDetail {
    
    
    private String tutorSkill;
    private String tutorSkillForTutorInformationFound;

    ArrayList<String> names = new ArrayList();
    
    
    
    
    public String getTutorSkill() {
        return tutorSkill;
    }

    
    
    public void setTutorSkill(String tutorSkill) {
        this.tutorSkill = tutorSkill;
    }

    public ArrayList<String> getNames() {
        return names;
    }

    
    public String getTutorSkillForTutorInformationFound() {
        return tutorSkillForTutorInformationFound;
    }

    
    public void setTutorSkillForTutorInformationFound(String tutorSkillForTutorInformationFound) {
        this.tutorSkillForTutorInformationFound = tutorSkillForTutorInformationFound;
    }

    
    
    public void setNames(ArrayList<String> names) {
        this.names = names;
    }

    
    
    @Override
    public String toString() {
        return "Report: " + "tutorSkill=" + tutorSkill;
    }
    
    
    
}

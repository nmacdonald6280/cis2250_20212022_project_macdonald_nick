package info.hccis.tutoring.entity;

import java.util.ArrayList;

/**
 * Class used for student reports.
 * @author fhm
 * @since 20211018
 */
public class StudentReport {

	private int id;
	ArrayList<Code> courses = new ArrayList();
	ArrayList<Student> students = new ArrayList();

	public ArrayList<Code> getCourses() {
		return courses;
	}

	public void setCourses(ArrayList<Code> courses) {
		this.courses = courses;
	}

	public ArrayList<Student> getStudents() {
		return students;
	}

	public void setStudents(ArrayList<Student> students) {
		this.students = students;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

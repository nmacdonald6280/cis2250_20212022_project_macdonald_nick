package info.hccis.tutoring.controllers;


import info.hccis.tutoring.entity.Tutor;
import info.hccis.tutoring.repositories.TutorRepository;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the Tutor functionality of the site
 *
 * @since 20211014
 * @author Carter Phillips
 */
@Controller
@RequestMapping("/tutor")
public class TutorController {

    private final TutorRepository _sr;

    @Autowired
    public TutorController(TutorRepository sr) {
        _sr = sr;
    }

    /**
     * Lists all Tutor
     *
     * @since 20211014
     * @author Carter Phillips
     */
    @RequestMapping("")
    public String list(Model model) {
        model.addAttribute("tutors", _sr.findAll());
        return "tutor/list";
    }

    /**
     * Page to add new Tutor
     *
     * @since 20211015
     * @author Carter Phillips
     */
    @RequestMapping("/add")
    public String add(Model model) {
        Tutor tutor = new Tutor();
        model.addAttribute("tutor", tutor);
        return "tutor/add";
    }

    /**
     * Submit method that processes add and edit and any form submission
     *
     * @since 20211015
     * @author Carter Phillips
     */
    @RequestMapping("/submit")
    public String submit(Model model, @Valid @ModelAttribute("tutor") Tutor tutor, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            System.out.println("Validation error");
            for (ObjectError error : bindingResult.getAllErrors()) {
                System.out.println(error.getDefaultMessage());
            }
            
            return "tutor/add";
        }

        _sr.save(tutor);
        model.addAttribute("tutors", _sr.findAll());
        return "tutor/list";
    }


    
    
    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable int id, Model model) {
        Optional<Tutor> tutor = _sr.findById(id);
        if (tutor != null) {
            model.addAttribute("tutor", tutor);
            return "tutor/edit";
        }
        return "/tutor/list";
    }
    
    
    /**
     * Page to delete a Tutor
     * @since 20211025
     * @author Carter Phillips
     */
    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable int id) {
        _sr.deleteById(id);
        return "redirect:/tutor";
    }

}

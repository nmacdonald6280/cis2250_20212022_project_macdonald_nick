package info.hccis.tutoring.rest;

/**
 * Custom Exception
 *
 * @author Alonso Briones
 * @since 20211122
 */
class AllAttributesNeededException extends Exception {

	public AllAttributesNeededException(String message) {
		super(message);
	}
}

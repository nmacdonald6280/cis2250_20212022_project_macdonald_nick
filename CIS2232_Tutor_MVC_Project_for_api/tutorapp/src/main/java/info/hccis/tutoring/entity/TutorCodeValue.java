package info.hccis.tutoring.entity;

/**
 * Represents a code value
 * @author Carter Phillips
 * @since 20211004
 */
public class TutorCodeValue {
    
    private int codeType;
    private int codeValueSequence;
    private String description;

    public TutorCodeValue(int codeType, int codeValueSequence, String description) {
        this.codeType = codeType;
        this.codeValueSequence = codeValueSequence;
        this.description = description;
    }

    public TutorCodeValue() {
    }

    
    
    public int getCodeType() {
        return codeType;
    }
    
    

    public void setCodeType(int codeType) {
        this.codeType = codeType;
    }

    public int getCodeValueSequence() {
        return codeValueSequence;
    }

    public void setCodeValueSequence(int codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}

package info.hccis.tutoring.controllers;

import info.hccis.tutoring.entity.Invoice;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import info.hccis.tutoring.repositories.InvoiceRepository;
import info.hccis.tutoring.repositories.TutorRepository;
import java.util.Optional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Controller for the student functionality of the site
 *
 * @since 20211014
 * @author CIS2232
 */
@Controller
@RequestMapping("/invoice")
public class InvoiceController {

    private final InvoiceRepository _ir;
    private final TutorRepository _tr;

    /**
     * Create Invoice repository and Tutor repository
     * 
     * @param ir
     * @param tr 
     */
    @Autowired
    public InvoiceController(InvoiceRepository ir, TutorRepository tr) {
        _ir = ir;
        _tr = tr;
    }

    /**
     * General mapping
     * 
     * @param model
     * @return 
     */
    @RequestMapping("")
    public String invoiceList(Model model) {
        model.addAttribute("invoices", _ir.findAll());
        return "invoice/invoiceList";
    }

    /**
     * Mapping for invoiceAdd
     * 
     * @param model
     * @return invoiceAdd
     */
    @RequestMapping("/invoiceAdd")
    public String invoiceAdd(Model model) {
        Invoice invoice = new Invoice();
        model.addAttribute("invoice", invoice);
        model.addAttribute("tutors", _tr.findAll());
        return "invoice/invoiceAdd";
    }

    /**
     * Mapping for submit on invoiceAdd. If there is an error it will just stay 
     * in the same invoiceAdd page else it will go back to the invoiceList
     * 
     * @param model
     * @param invoice
     * @param bindingResult
     * @return 
     */
    @RequestMapping("/submit")
    public String submit(Model model, @Valid @ModelAttribute("invoice") Invoice invoice, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println("Validation error");
            for (ObjectError error : bindingResult.getAllErrors()) {
                System.out.println(error.getDefaultMessage());
            }
            model.addAttribute("tutors", _tr.findAll());
            return "invoice/invoiceAdd";
        }
        _ir.save(invoice);
        model.addAttribute("invoices", _ir.findAll());
        
        
        return "invoice/invoiceList";
    }

    /**
     * Mapping for delete invoice 
     * 
     * @param id
     * @return 
     */
    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable int id) {
        _ir.deleteById(id);
        return "redirect:/invoice";
    }

    /**
     * Mapping for editing an invoice
     * 
     * @param id
     * @param model
     * @return 
     */
    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable int id, Model model) {
        Optional<Invoice> invoice = _ir.findById(id);

        model.addAttribute("invoice", invoice);
        model.addAttribute("tutors", _tr.findAll());
        return "invoice/invoiceAdd";
    }
}

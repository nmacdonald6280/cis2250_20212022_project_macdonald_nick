package info.hccis.tutoring.rest;

import com.google.gson.Gson;
import info.hccis.tutoring.entity.Tutor;
import info.hccis.tutoring.repositories.TutorRepository;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

//Author Carter Phillips

@Path("/TutorService/tutor")
public class TutorService {

    private final TutorRepository _sr;

    @Autowired
    public TutorService(TutorRepository _sr) {
        this._sr = _sr;
    }
    @GET
    @Produces("application/json")
    public ArrayList<Tutor> getAll() {
        ArrayList<Tutor> tutors = (ArrayList<Tutor>) _sr.findAll();
        return tutors;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getTutorById(@PathParam("id") int id) throws URISyntaxException {

        Optional<Tutor> tutor = _sr.findById(id);

        if (!tutor.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(tutor).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTutor(String tutorJson) 
    {        
        try{
            String temp = save(tutorJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
    
    @DELETE
    @Path("/{id}")
    public Response deleteTutor(@PathParam("id") int id) throws URISyntaxException {
        Optional<Tutor> tutor = _sr.findById(id);
        if(tutor != null) {
            _sr.deleteById(id);
            return Response.status(HttpURLConnection.HTTP_CREATED).build();
        }
        return Response.status(404).build();
    }

    
    
//    @PUT
//    @Path("/{id}")
//    @Consumes("application/json")
//    @Produces("application/json")
//    public Response updateCamper(@PathParam("id") int id, String camperJson) throws URISyntaxException 
//    {
//
//        try{
//            String temp = save(camperJson);
//            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
//                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
//        }catch(AllAttributesNeededException aane){
//            return Response.status(400).entity(aane.getMessage()).build();
//        }
//
//    }

    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        Tutor tutor = gson.fromJson(json, Tutor.class);
        
//        if(camper.getFirstName() == null || camper.getFirstName().isEmpty()) {
//            throw new AllAttributesNeededException("Please provide all mandatory inputs");
//        }
 
        if(tutor.getId() == null){
            tutor.setId(0);
        }

        tutor = _sr.save(tutor);

        String temp = "";
        temp = gson.toJson(tutor);

        return temp;
        
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.tutoring.bo;

import info.hccis.tutoring.dao.TutorCodeValueDAO;
import info.hccis.tutoring.entity.TutorCodeValue;
import java.util.ArrayList;

/**
 *
 * @author Carter Phillips
 */
public class TutorCodeValueBO {

    /**
     * Select all code values based on code type
     *
     * @since 20210924
     * @author Carter Phillips
     */
    public ArrayList<TutorCodeValue> selectCodeValues(int codeTypeId) {
        TutorCodeValueDAO codeValueDAO = new TutorCodeValueDAO();
        return codeValueDAO.selectCodeValues(codeTypeId);
    }
}
